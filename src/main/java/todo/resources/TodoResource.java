package todo.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import todo.api.Todo;

import java.net.URISyntaxException;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;

import todo.dao.TodoDAO;
import todo.service.TodoService;


@Path("/todo")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TodoResource {
    private final TodoService todoService;

    public TodoResource(TodoService todoService) {
        this.todoService = todoService;
    }

    @GET
    public Todo get(@QueryParam("name") Optional<String> name, @QueryParam("val") Optional<String> val) {
        Todo todo = new Todo();
        todo.setName(name.orElse("anonymous"));
        todo.setTodo(val.orElse("something"));
        return todo;
    }

    @POST
    public Response post(Todo todo) throws URISyntaxException {
        todoService.add(todo);
        return null;
    }

}
