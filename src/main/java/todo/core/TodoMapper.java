package todo.core;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import todo.api.Todo;

public class TodoMapper implements RowMapper<Todo> {

    public TodoMapper() {
    }

    @Override
    public Todo map(ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Todo(resultSet.getInt("id"),
                resultSet.getString("name"),
                resultSet.getString("email"),
                resultSet.getString("todo")
        );
    }
}
