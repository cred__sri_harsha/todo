
package todo.dao;


import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import todo.core.TodoMapper;
import todo.api.Todo;

@RegisterRowMapper(TodoMapper.class)
public interface TodoDAO {

    @SqlQuery("select * from todo")
    List<Todo> getAll();

    @SqlQuery("select * from todo where id = :id")
    Todo getById(@Bind("id") int id);

    @SqlUpdate("insert into todo (name, todo, email) values (:name, :todo, :email)")
    int add(@BindBean Todo todo);
}