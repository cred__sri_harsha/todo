package todo.service;

import todo.api.Todo;
import todo.dao.TodoDAO;

import java.util.List;

public class TodoService {
    private TodoDAO todoDAO;

    public TodoService() {
    }

    public TodoService(TodoDAO todoDAO) {
        this.todoDAO = todoDAO;
    }

    public List<Todo> getAll() {
        return todoDAO.getAll();
    }

    public Todo getById(int id) {
        return todoDAO.getById(id);
    }

    public int add(Todo todo) {
        todoDAO.add(todo);
        return 1;
    }
}
