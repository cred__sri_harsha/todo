package todo.api;

public class Todo {
    private String email;
    private int id;
    private String name;
    private String todo;

    public Todo() {
    }

    public Todo(String name, String todo) {
        this.name = name;
        this.todo = todo;
    }

    public Todo(int id, String name, String email, String todo) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.todo = todo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }
}
