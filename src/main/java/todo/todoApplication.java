package todo;

import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import todo.api.Todo;
import todo.dao.TodoDAO;
import todo.resources.TodoResource;
import org.jdbi.v3.core.Jdbi;
import io.dropwizard.setup.Environment;
import todo.service.TodoService;

public class todoApplication extends Application<todoConfiguration> {

    public static void main(final String[] args) throws Exception {
        new todoApplication().run(args);
    }

    @Override
    public String getName() {
        return "todo";
    }

    @Override
    public void initialize(final Bootstrap<todoConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final todoConfiguration configuration,
                    final Environment environment) {
//        final TodoResource resource = new TodoResource();
//        environment.jersey().register(resource);
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        final TodoDAO todoDAO = jdbi.onDemand(TodoDAO.class);
        final TodoResource todoResource = new TodoResource(new TodoService(todoDAO));
        environment.jersey().register(todoResource);
    }
}
